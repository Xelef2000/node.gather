import cv2
import numpy as np
import socket
import sys
import pickle
import struct
import numpy as np
from mss import mss
from PIL import Image

bbox = {'top': 0, 'left': 0, 'width': 1280, 'height': 1024}

sct = mss()

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('192.168.220.52', 8485))
connection = client_socket.makefile('wb')


img_counter = 0

encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

while True:
    sct_img = sct.grab(bbox)
    sct_img = np.array(sct_img)
    frame = sct_img
    frame = cv2.imencode('.jpg', frame, encode_param)
#    data = zlib.compress(pickle.dumps(frame, 0))
    data = pickle.dumps(frame, 0)
    size = len(data)

    print("{}: {}".format(img_counter, size))
    client_socket.sendall(struct.pack(">L", size) + data)
    img_counter += 1
