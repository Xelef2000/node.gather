from vncdotool import api
import cv2
import numpy as np
import time

client = api.connect('172.17.0.2::5901', password="vncpassword")



while 1:
    client.captureScreen('screen.png')
    img = cv2.imread('screen.png',1)
    cv2.imshow('image', np.array(img))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break

