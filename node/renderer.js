var ById = function(id) {
    return document.getElementById(id);
}
const jsonfile = require('jsonfile');
const favicon = require('favicon-getter').default;
const path = require('path');
const uuid = require('uuid');
const bookmarks = path.join(__dirname, 'bookmarks.json');
const {
    remote
} = require('electron');

/*
var back = ById('back'),
    forward = ById('forward'),
    refresh = ById('refresh'),
    omni = ById('url'),
    dev = ById('console'),
    fave = ById('favea
    list = ById('list'),
    popup = ById('fave-popup'),
    view = ById('view'),
    trigger1 = ById('trigger1'),
    trigger2 = ById('trigger2');

*/

var view = ById('view');

function reloadView() {
    view.reload();
}

function backView() {
    view.goBack();
}

function forwardView() {
    view.goForward();
}

function updateURL(event) {
    if (event.keyCode === 13) {
        omni.blur();
        let val = omni.value;
        let https = val.slice(0, 8).toLowerCase();
        let http = val.slice(0, 7).toLowerCase();
        if (https === 'https://') {
            view.loadURL(val);
        } else if (http === 'http://') {
            view.loadURL(val);
        } else {
            view.loadURL('http://' + val);
        }
    }
}

function goToURL(url) {
    let val = url;
    let https = val.slice(0, 8).toLowerCase();
    let http = val.slice(0, 7).toLowerCase();
    if (https === 'https://') {
        view.loadURL(val);
    } else if (http === 'http://') {
        view.loadURL(val);
    } else {
        view.loadURL('http://' + val);
    }
}

var Bookmark = function(id, url, faviconUrl, title) {
    this.id = id;
    this.url = url;
    this.icon = faviconUrl;
    this.title = title;
}

Bookmark.prototype.ELEMENT = function() {
    var a_tag = document.createElement('a');
    a_tag.href = this.url;
    a_tag.className = 'link';
    a_tag.textContent = this.title;
    var favimage = document.createElement('img');
    favimage.src = this.icon;
    favimage.className = 'favicon';
    a_tag.insertBefore(favimage, a_tag.childNodes[0]);
    return a_tag;
}

function addBookmark() {
    let url = view.src;
    let title = view.getTitle();
    favicon(url).then(function(fav) {
        let book = new Bookmark(uuid.v1(), url, fav, title);
        jsonfile.readFile(bookmarks, function(err, curr) {
            curr.push(book);
            jsonfile.writeFile(bookmarks, curr, function(err) {})
        })
    })
}

function openPopUp(event) {
    let state = popup.getAttribute('data-state');
    if (state === 'closed') {
        popup.innerHTML = '';
        jsonfile.readFile(bookmarks, function(err, obj) {
            if (obj.length !== 0) {
                for (var i = 0; i < obj.length; i++) {
                    let url = obj[i].url;
                    let icon = obj[i].icon;
                    let id = obj[i].id;
                    let title = obj[i].title;
                    let bookmark = new Bookmark(id, url, icon, title);
                    let el = bookmark.ELEMENT();
                    popup.appendChild(el);
                }
            }
            popup.style.display = 'block';
            popup.setAttribute('data-state', 'open');
        });
    } else {
        popup.style.display = 'none';
        popup.setAttribute('data-state', 'closed');
    }
}

function handleUrl(event) {
    if (event.target.className === 'link') {
        event.preventDefault();
        view.loadURL(event.target.href);
    } else if (event.target.className === 'favicon') {
        event.preventDefault();
        view.loadURL(event.target.parentElement.href);
    }
}

function handleDevtools() {
    if (view.isDevToolsOpened()) {
        view.closeDevTools();
    } else {
        view.openDevTools();
    }
}

function updateNav(event) {
    omni.value = view.src;
}
/*
refresh.addEventListener('click', reloadView);
back.addEventListener('click', backView);
forward.addEventListener('click', forwardView);
omni.addEventListener('keydown', updateURL);
fave.addEventListener('click', addBookmark);
list.addEventListener('click', openPopUp);
popup.addEventListener('click', handleUrl);
dev.addEventListener('click', handleDevtools);
view.addEventListener('did-finish-load', updateNav);

trigger1.addEventListener('click', function(){
    fullscreenScreenshot( 'image/png');
});

trigger2.addEventListener('click', function(){
    appScreenshot('image/png');
});

*/

const {
    desktopCapturer
} = require('electron');
const fs = require('fs');
const base64ToImage = require('base64-to-image');
const net = require('net');
const async = require('async');
const appImgName = 'imgs/app-'
const fullScrennImgName = 'imgs/fs-'
/**
 * Create a screenshot of the entire screen using the desktopCapturer module of Electron.
 *
 * @param callback {Function} callback receives as first parameter the base64 string of the image
 * @param imageFormat {String} Format of the image to generate ('image/jpeg' or 'image/png')
 **/
function fullscreenScreenshot(on_done, imageFormat) {
    var _this = this;
    imageFormat = imageFormat || 'image/jpeg';

    this.handleStream = (stream) => {
        // Create hidden video tag
        var video = document.createElement('video');
        video.style.cssText = 'position:absolute;top:-10000px;left:-10000px;';

        // Event connected to stream
        video.onloadedmetadata = function() {
            // Set video ORIGINAL height (screenshot)
            video.style.height = this.videoHeight + 'px'; // videoHeight
            video.style.width = this.videoWidth + 'px'; // videoWidth

            video.play();

            // Create canvas
            var canvas = document.createElement('canvas');
            canvas.width = this.videoWidth;
            canvas.height = this.videoHeight;
            var ctx = canvas.getContext('2d');
            // Draw video on canvas
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            let img = canvas.toDataURL();
            on_done(img);
            // Remove hidden video tag
            video.remove();
            try {
                // Destroy connect to stream
                stream.getTracks()[0].stop();
            } catch (e) {}
        }

        video.srcObject = stream;
        document.body.appendChild(video);
    };

    this.handleError = function(e) {
        console.log(e);
    };

    desktopCapturer.getSources({
        types: ['window', 'screen']
    }).then(async sources => {
        console.log(sources);

        for (const source of sources) {
            // Filter: main screen
            if ((source.name === "Entire screen") || (source.name === "Screen 1") || (source.name === "Screen 2")) {
                try {
                    const stream = await navigator.mediaDevices.getUserMedia({
                        audio: false,
                        video: {
                            mandatory: {
                                chromeMediaSource: 'desktop',
                                chromeMediaSourceId: source.id,
                                minWidth: 1280,
                                maxWidth: 4000,
                                minHeight: 720,
                                maxHeight: 4000
                            }
                        }
                    });

                    _this.handleStream(stream);
                } catch (e) {
                    _this.handleError(e);
                }
            }
        }
    });
}

function appScreenshot(on_done, imageFormat) {
    var _this = this;
    imageFormat = imageFormat || 'image/jpeg';

    this.handleStream = (stream) => {
        // Create hidden video tag
        var video = document.createElement('video');
        video.style.cssText = 'position:absolute;top:-10000px;left:-10000px;';
        // Event connected to stream
        video.onloadedmetadata = function() {
            // Set video ORIGINAL height (screenshot)
            video.style.height = this.videoHeight + 'px'; // videoHeight
            video.style.width = this.videoWidth + 'px'; // videoWidth

            video.play();

            // Create canvas
            var canvas = document.createElement('canvas');
            canvas.width = this.videoWidth;
            canvas.height = this.videoHeight;
            var ctx = canvas.getContext('2d');
            // Draw video on canvas
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            let img = canvas.toDataURL();
            on_done(img);
            // Remove hidden video tag
            video.remove();

            try {
                // Destroy connect to stream
                stream.getTracks()[0].stop();
            } catch (e) {}
        }

        video.srcObject = stream;
        document.body.appendChild(video);
    };

    this.handleError = function(e) {
        console.log(e);
    };

    desktopCapturer.getSources({
        types: ['window', 'screen']
    }).then(async sources => {
        console.log(sources);

        for (const source of sources) {
            // Filter: main screen
            if (source.name === document.title) {
                try {
                    const stream = await navigator.mediaDevices.getUserMedia({
                        audio: false,
                        video: {
                            mandatory: {
                                chromeMediaSource: 'desktop',
                                chromeMediaSourceId: source.id,
                                minWidth: 1280,
                                maxWidth: 4000,
                                minHeight: 720,
                                maxHeight: 4000
                            }
                        }
                    });

                    _this.handleStream(stream);
                } catch (e) {
                    _this.handleError(e);
                }
            }
        }
    });
}




var server = net.createServer(function(socket) {
    socket.on('data', data => {
        exec(data.toString(), (result) => {
            let length = result.length
            socket.write(result);
        });
    });
});

server.listen(1337);

//TODO: implm press lift down up


function exec(data, on_done) {
    returnVal = 'ok'
    res = data.split(";");
    console.log(res);
    res.forEach(function(item, index) {
        //console.log(item, index);
        switch (item) {
            case 'reload':
                reloadView();
                returnVal = returnVal + ';ok';
                on_done(returnVal);
                break;
            case 'back':
                backView();
                returnVal = returnVal + ';ok';
                on_done(returnVal);
                break;
            case 'forward':
                forwardView();
                returnVal = returnVal + ';ok';
                on_done(returnVal);
                break;
            case 'appScreen':
                appScreenshot(x => on_done("imgA*" + x), "image/jpg");
                returnVal = returnVal + ';ok';
                break;
            case 'fullScreen':
                fullscreenScreenshot(x => on_done("imgF*" + x), "image/jpg");
                returnVal = returnVal + ';ok';
                break;
            case 'ping':
                returnVal = 'pong';
                on_done(returnVal);
                break;
            default:
                if (item.includes('goToUrl*')) {
                    url = item.split('*')[1];
                    console.log(url)
                    goToURL(url);
                    returnVal = returnVal + ';ok';
                    on_done(returnVal);
                } else if (item.includes('press*')) {
                    let win = remote.getCurrentWindow();
                    let key = item.split('*')[1];
                    console.log('press:'+key);
                    win.webContents.sendInputEvent({type: 'keyDown', keyCode: 'g'})
                    win.webContents.sendInputEvent({type: 'char', keyCode: 'g'})
                    win.webContents.sendInputEvent({type: 'keyDown', keyCode: 'k'})
                    win.webContents.sendInputEvent({type: 'char', keyCode: 'k'})
                    returnVal = returnVal + ';ok';
                    on_done(returnVal);

                } else if (item.includes('down*')) {

                } else if (item.includes('up*')) {

                } else {
                    returnVal = returnVal + ';no valid command!';
                    on_done(returnVal);
                }

        }
    });
}



